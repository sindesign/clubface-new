<?php
	/* Load our blocks up here so we don't hit the database more than we have to */
	$buildBlocks = [];
	foreach($blocks as $blockId) {
		$blockData = \ClubFace\Block::findById($blockId, \ClubFace\Account::current()->_id);
		if (is_null($blockData)) {
			continue;
		}

		$buildBlocks[] = \ClubFace\Block::getBlockClass($blockData->class)->setConfiguration($blockData->configuration);
	}
?>
<html>
	<head>
		<title>{{$title}}</title>
		<link href="//clubface.com.au/themes/{{\ClubFace\Account::current()->theme}}/style.css" rel='stylesheet' type='text/css'>
		@if (!is_null($css_file))
			<link href="//clubface.com.au/themes/{{$account_id}}/{{$css_file}}" rel='stylesheet' type='text/css'>
		@endif
		@if (!is_null($css_overrides))
		<style>
			{{$css_overrides}}
		</style>
		@endif
		{{-- Render anything that needs to go in the head --}}
		@foreach($buildBlocks as $block)
			{!! $block->renderHead() !!}
		@endforeach
	</head>
	<body>
		{{-- Render anything that needs to go in the body --}}
		@foreach($buildBlocks as $block)
			{!! $block->renderBody() !!}
		@endforeach

		{{-- Render anything that should be non-blocking --}}
		@foreach($buildBlocks as $block)
			{!! $block->renderEnd() !!}
		@endforeach
	</body>
</html>
