<html>
	<head>
		<title>Laravel</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">Login</div>
				<form method="POST" id="login-form" action="{{url('/auth/login')}}">
					<input name="_token" type="hidden" value="{{csrf_token()}}">
					<input autocapitalize="off" name="email" id="email-input-field" type="text" placeholder="Email">
					<br />
					<input name="password" type="password" placeholder="Password">
					<br /><br />
					<input id="submit-button" type="submit" value="Login" name="submit">
				</form>
			</div>
		</div>
	</body>
</html>
