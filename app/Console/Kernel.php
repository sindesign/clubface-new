<?php namespace ClubFace\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'ClubFace\Console\Commands\DBInit',
		'ClubFace\Console\Commands\DBDrop',
		'ClubFace\Console\Commands\Dummy',
	];
}
