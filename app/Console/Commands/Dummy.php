<?php namespace ClubFace\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Dummy extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dummy';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Populates the database with dummy data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		/* Ensure that we only ever run on a development environment */
		if (\App::environment() != 'local' && \App::environment() != 'testing') {
			$this->error(PHP_EOL . 'This command can only be run in a development or testing environment.' . PHP_EOL);
			return -1;
		}

		echo PHP_EOL;
		$this->info('Hit enter for defaults: testclub.com.au, developer@clubface.com.au and meowmeow');
		echo PHP_EOL;

		/* Collect the data required for an account */
		$domain = '';
		while((preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain)
            && preg_match("/^.{1,253}$/", $domain)
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain)) === false) {
			$domain = $this->ask('Enter the domain to use (without www.):');
			if (strlen($domain) == 0) {
				$domain = 'testclub.com.au';
			}
		}
		$domain = str_replace('www.', '', $domain);

		$email = '';
		while (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$email = $this->ask('Enter the email address to use for this account:');
			if (strlen($email) == 0) {
				$email = 'developer@clubface.com.au';
			}
		}

		$password = '';
		while (strlen($password) < 6) {
			$password = $this->ask('Enter the password to use for the new user (6 or more chars):');
			if (strlen($password) == 0) {
				$password = 'meowmeow';
			}
		}

		/* Fill the database with stuff */
		$account = \ClubFace\Account::create([
			'name' => 'Development Golf Club',
			'domain' => 'www.' . $domain,
			'domains' => [
				'www.' . $domain,
				$domain,
			],
			'owner' => '',
		]);

		$user = \ClubFace\User::create([
			'account_id' => $account->_id,
			'name' => 'Developer',
			'email' => $email,
			'password' => $password,
		]);

		$account->owner = $user->_id;
		$account->save();

		$page = \ClubFace\Page::create([
			'account_id' => $account->_id,
			'url' => '/',
			'title' => 'Development Club Homepage',
			'social' => [],
			'blocks' => [],
			'created_by' => $user->_id,
		]);



		echo PHP_EOL . 'Created dummy data for: ' . $domain . PHP_EOL;
	}
}
