<?php namespace ClubFace\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DBDrop extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dbdrop';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Removes all tables in the MongoDB Database';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		/* Ensure that we only ever run on a development environment */
		if (\App::environment() != 'local') {
			$this->error(PHP_EOL . 'DBInit can only be run in a local/development environment.' . PHP_EOL);
			return -1;
		}

		/* Go ahead and add tables that don't already exist */
		if (\Schema::hasTable('accounts')) {
			\Schema::drop('accounts');
		}

		if (\Schema::hasTable('users')) {
			\Schema::drop('users');
		}

		if (\Schema::hasTable('sessions')) {
			\Schema::drop('sessions');
		}

		if (\Schema::hasTable('mutex')) {
			\Schema::drop('mutex');
		}

		if (\Schema::hasTable('redirects')) {
			\Schema::drop('redirects');
		}

		if (\Schema::hasTable('blocks')) {
			\Schema::drop('blocks');
		}

		if (\Schema::hasTable('pages')) {
			\Schema::drop('pages');
		}
	}
}
