<?php namespace ClubFace\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DBInit extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dbinit';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Initialises the MongoDB Database';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		/* Ensure that we only ever run on a development environment */
		if (\App::environment() != 'local') {
			$this->error(PHP_EOL . 'DBInit can only be run in a local/development environment.' . PHP_EOL);
			return -1;
		}

		/* Go ahead and add tables that don't already exist */
		if (!\Schema::hasTable('accounts')) {
			\Schema::create('accounts', function($table)
			{
			    $table->index('domain');
			    $table->index('domains');
			});
		}

		if (!\Schema::hasTable('users')) {
			\Schema::create('users', function($table)
			{
			    $table->index('account_id');
			    $table->index('email');
			});
		}

		if (!\Schema::hasTable('sessions')) {
			\Schema::create('sessions', function($table) {});
		}

		if (!\Schema::hasTable('mutex')) {
			\Schema::create('mutex', function($table)
			{
			    $table->index('key');
			});
		}

		if (!\Schema::hasTable('redirects')) {
			\Schema::create('redirects', function($table)
			{
			    $table->index('account_id');
			    $table->index('from');
			});
		}

		if (!\Schema::hasTable('blocks')) {
			\Schema::create('blocks', function($table)
			{
			    $table->index('account_id');
			});
		}

		if (!\Schema::hasTable('pages')) {
			\Schema::create('pages', function($table)
			{
			    $table->index('account_id');
			    $table->index('url');
			});
		}
	}
}
