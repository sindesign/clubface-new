<?php namespace ClubFace\Database;

use Carbon\Carbon;
use ClubFace\Constants;

class Model extends \Jenssegers\MongoDB\Model {

    public function __construct()
    {
        parent::__construct();
    }

	public function getCreatedAtAttribute($value)
    {
    	return (int) Carbon::parse($value)->format('U');
    }

    public function getUpdatedAtAttribute($value)
    {
    	return (int) Carbon::parse($value)->format('U');
    }

    public function getDeletedAtAttribute($value)
    {
    	return (int) Carbon::parse($value)->format('U');
    }

    public static function findById($id, $accountId)
    {
        return self::where('_id', '=', $id)
            ->where('account_id', '=', $accountId)
            ->first();
    }
    
}
