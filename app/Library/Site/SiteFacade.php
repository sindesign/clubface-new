<?php namespace ClubFace;

use Illuminate\Support\Facades\Facade;

class SiteFacade extends Facade {

	protected static function getFacadeAccessor() { return 'site'; }

}
