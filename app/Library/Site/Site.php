<?php namespace ClubFace;

class Site {

	function marshall()
	{
		/* Firstly, find out what account we're using */
		$account = \ClubFace\Account::findByDomain(\Request::server('HTTP_HOST'));
		if (is_null($account)) {
			echo 'TODO: SHOW NO ACCOUNT EXISTS PAGE HERE'; exit; // TODO
		}

		/* Set the request's current account */
		\ClubFace\Account::current($account);
		unset($account);

		/* If we're on a domain that is different to the offical one, redirect now. */
		if (\ClubFace\Account::current()->domain != \Request::server('HTTP_HOST')) {
			return \Redirect::away('http://' . \ClubFace\Account::current()->domain . '/' . ltrim(\Request::path(), '/'), 301);
		}

		/* If the account is currently disabled for any reason, show the account suspension page here */
		if (\ClubFace\Account::current()->suspended) {
			echo 'TODO: SHOW THE ACCOUNT SUSPENDED PAGE HERE'; exit; // TODO
		}

		/* See if this request should redirect somewhere else */
		$redirect = \ClubFace\Redirect::findByPath(\Request::path(), \ClubFace\Account::current()->_id);
		if (!is_null($redirect)) {
			return \Redirect::away((is_null($redirect->newdomain) ?
				'http://' . \ClubFace\Account::current()->domain : $redirect->newdomain) . '/' . $redirect->newpath,
				$redirect->code);
		}

		/* Return the current page */
		return \ClubFace\Page::render(\Request::path());
	}

}
