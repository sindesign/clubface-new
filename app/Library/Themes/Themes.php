<?php namespace ClubFace;

class Themes {

	const ONE = 'One';

	private static $themes = [
		self::ONE => '\ClubFace\Themes\One',
	];

	public static function getThemes()
	{
		return self::$themes;
	}

	public static function isValid($theme)
	{
		return array_key_exists($theme, self::getThemes());
	}

	public static function getTheme($theme)
	{
		if (!self::isValid($theme)) {
			return null;
		}

		return new self::$themes[$theme];
	}

	public static function getCurrentTheme()
	{
		return self::getTheme(\ClubFace\Account::current()->theme);
	}

}
