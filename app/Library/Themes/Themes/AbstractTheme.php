<?php namespace ClubFace\Themes;

abstract class AbstractTheme {

	const ID = null;
	const NAME = null;
	const LONG_DESC = null;
	const SHORT_DESC = null;

	/**
	 * Return an array of blocks that are supported by this theme
	 *
	 * @return array
	 */
	abstract public function getSupportedBlocks();

	public function blockIsSupported($blockId) {
		return in_array($blockId, $this->getSupportedBlocks()); 
	}

	public function getId()
	{
		return static::ID;
	}

}
