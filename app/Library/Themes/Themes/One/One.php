<?php namespace ClubFace\Themes;

class One extends \ClubFace\Themes\AbstractTheme {

	const ID = \ClubFace\Themes::ONE;
	const NAME = ' Theme One';
	const LONG_DESC = 'Long Theme Desc';
	const SHORT_DESC = 'Short Theme Desc';

	/**
	 * Return an array of blocks that are supported by this theme
	 *
	 * @return array
	 */
	public function getSupportedBlocks() {
		return [
			\ClubFace\Blocks\MapsFullWidth::ID,
			\ClubFace\Blocks\BasicContent::ID,
		];
	}

}
