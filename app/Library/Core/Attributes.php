<?php namespace ClubFace\Core;

class Attributes {

	/* Supported Attributes */
	const CONTACT_NUMBER = 0;

	/* Helper Functions */
	private static getDataSet($type)
	{
		$dataSet = null;
		switch ($type) {
			case self::CONTACT_NUMBER:
				$dataSet = self::getContactNumberTypes();
				break;
			default:
				$dataSet = null;
				break;
		}

		return $dataSet;
	}

	public static function isValid($type, $value)
	{
		$dataSet = self::getDataSet($type);

		if (is_null($dataSet)) {
			return false;
		}

		foreach ($dataSet as $data) {
			if ($data['val'] === $value) {
				return true;
			}
		}

		return false;
	}

	public static function getLongDesc($type, $value)
	{
		$dataSet = self::getDataSet($type);

		if (is_null($dataSet)) {
			return '';
		}

		foreach ($dataSet as $data) {
			if ($data['id'] == $value) {
				return $data['long_desc'];
			}
		}

		return '';
	}

	public static function getShortDesc($type, $value)
	{
		$dataSet = self::getDataSet($type);

		if (is_null($dataSet)) {
			return '';
		}

		foreach ($dataSet as $data) {
			if ($data['id'] == $value) {
				return $data['short_desc'];
			}
		}

		return '';
	}

	/* Contact Numbers */
	const CONTACT_NUMBER_MOBILE = 0;
	const CONTACT_NUMBER_PHONE = 1;
	const CONTACT_NUMBER_FAX = 2;

	public static function getContactNumberTypes()
	{
		return [
			['id' => self::CONTACT_NUMBER_MOBILE, 'long_desc' => 'Mobile', 'short_desc' => 'M'],
			['id' => self::CONTACT_NUMBER_PHONE, 'long_desc' => 'Phone', 'short_desc' => 'P'],
			['id' => self::CONTACT_NUMBER_FAX, 'long_desc' => 'Fax', 'short_desc' => 'F'],
		];
	}

}
