<?php namespace ClubFace\Blocks;

 class MapsFullWidth extends \ClubFace\AbstractBlock {

 	const ID = 'MapsFullWidth';
 	const NAME = 'Full width maps display';
 	const LONG_DESC = 'LONG DESCRIPTION';
 	const SHORT_DESC = 'SHORT DESCRIPTION';

	/**
	 * Return the name of the current block
	 *
	 * @return string
	 */
	public function getName() {
		return self::NAME;
	}

	/**
	 * Return the url for the JS to load for the administration of this block
	 *
	 * @return string
	 */
	public function getAdminInterface() {
		
	}

	/**
	 * Return whether or not the POST input is valid for this block
	 *
	 * This should always be run before buildConfig()
	 *
	 * @return bool
	 */
	public function runInputCheck() {
		
	}

	/**
	 * This will return the default configuration for a block
	 *
	 * @return array
	 */
	public function getDefaultConfig() {
		return [
			'lat' => '-26.4390742',
			'long' => '133.281323',
			'mapOptions' => [
				'zoom' => 4,
				'scrollWheelEnabled' => 'false',
			],
			'infoWindow' => [
				'enabled' => true,
				'showAddress' => true,
				'description' => '',
			],
		];
	}

	/**
	 * This will build a configuration array based on the input from a POST request
	 *
	 * runInputCheck() must always run before doing this
	 *
	 * @return array
	 */
	public function buildConfig() {
		
	}

	/**
	 * Generate HTML that is required in the head tag
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	public function renderHead() {
		return '';
	}

	/**
	 * Generate HTML that is required in the body tag
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	public function renderBody() {
		return '<section id="map"><div id="map-canvas"></div></section>';
	}

	/**
	 * Generate HTML that is required at the end of a page
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	public function renderEnd() {
		$output = '
		<script src="https://maps.googleapis.com/maps/api/js"></script>
		<script>
			function initialize() {
				var myLatlng = new google.maps.LatLng(' . $this->currentConfiguration['lat'] . ',' . $this->currentConfiguration['long'] . ');
				var mapOptions = {
					zoom: ' . $this->currentConfiguration['mapOptions']['zoom'] . ',
					center: myLatlng,
					scrollwheel: ' . $this->currentConfiguration['mapOptions']['scrollWheelEnabled'] . '
				}
				var map = new google.maps.Map(document.getElementById(\'map-canvas\'), mapOptions);
		';

		if ($this->currentConfiguration['infoWindow']['enabled']) {
			$output .= '
				var infowindow = new google.maps.InfoWindow({
					content: "<b>' . \ClubFace\Account::current()->club['name'] . '</b>"
				});
			';
		}

		$output .= '
			var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: "' . \ClubFace\Account::current()->club['name'] . '"
				});

				google.maps.event.addListener(marker, \'click\', function() {
					infowindow.open(map,marker);
				});
			}

			google.maps.event.addDomListener(window, \'load\', initialize);
		</script>
		';
		return $output;
	}

}