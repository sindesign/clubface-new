<?php namespace ClubFace\Blocks;

 class BasicContent extends \ClubFace\AbstractBlock {

 	const ID = 'BasicContent';
 	const NAME = 'Basic Page Content';
 	const LONG_DESC = 'LONG DESCRIPTION';
 	const SHORT_DESC = 'SHORT DESCRIPTION';

	/**
	 * Return the name of the current block
	 *
	 * @return string
	 */
	public function getName() {
		return self::NAME;
	}

	/**
	 * Return the url for the JS to load for the administration of this block
	 *
	 * @return string
	 */
	public function getAdminInterface() {
		
	}

	/**
	 * Return whether or not the POST input is valid for this block
	 *
	 * This should always be run before buildConfig()
	 *
	 * @return bool
	 */
	public function runInputCheck() {
		
	}

	/**
	 * This will return the default configuration for a block
	 *
	 * @return array
	 */
	public function getDefaultConfig() {
		return [
			'content' => '',
		];
	}

	/**
	 * This will build a configuration array based on the input from a POST request
	 *
	 * runInputCheck() must always run before doing this
	 *
	 * @return array
	 */
	public function buildConfig() {
		
	}

	/**
	 * Generate HTML that is required in the head tag
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	public function renderHead() {
		return '';
	}

	/**
	 * Generate HTML that is required in the body tag
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	public function renderBody() {
		return $this->currentConfiguration['content'];
	}

	/**
	 * Generate HTML that is required at the end of a page
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	public function renderEnd() {
		return '';
	}

}