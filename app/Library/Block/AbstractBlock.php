<?php namespace ClubFace;

abstract class AbstractBlock {

	const ID = null;
	const NAME = null;
	const LONG_DESC = null;
	const SHORT_DESC = null;

	protected $currentConfiguration = [];

	/**
	 * Return the name of the current block
	 *
	 * @return string
	 */
	abstract public function getName();

	/**
	 * Return the url for the JS to load for the administration of this block
	 *
	 * @return string
	 */
	abstract public function getAdminInterface();

	/**
	 * Return whether or not the POST input is valid for this block
	 *
	 * This should always be run before buildConfig()
	 *
	 * @return bool
	 */
	abstract public function runInputCheck();

	/**
	 * This will return the default configuration for a block
	 *
	 * @return array
	 */
	abstract public function getDefaultConfig();

	/**
	 * This will build a configuration array based on the input from a POST request
	 *
	 * runInputCheck() must always run before doing this
	 *
	 * @return array
	 */
	abstract public function buildConfig();

	/**
	 * Generate HTML that is required in the head tag
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	abstract public function renderHead();

	/**
	 * Generate HTML that is required in the body tag
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	abstract public function renderBody();

	/**
	 * Generate HTML that is required at the end of a page
	 *
	 * buildConfig() has to be run at least once for this method to work
	 *
	 * @return string
	 */
	abstract public function renderEnd();

	public function setConfiguration($config)
	{
		$this->currentConfiguration = $config;
		return $this;
	}

}