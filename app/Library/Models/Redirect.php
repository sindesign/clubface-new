<?php namespace ClubFace;

class Redirect extends \ClubFace\Database\Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'redirects';

	public static function create(array $arguments)
	{
		if (!is_array($arguments) || !array_key_exists('account_id', $arguments) || !array_key_exists('from', $arguments) ||
			!array_key_exists('to', $arguments) || !array_key_exists('created_by', $arguments)) {
			return null;
		}

		$redirect = new \ClubFace\Redirect;
		$redirect->account_id = (string) $arguments['account_id'];
		$redirect->from = (string) $arguments['from'];
		$redirect->to = (string) $arguments['to'];
		$redirect->active = true;
		$redirect->created_by = (string) $arguments['created_by'];
		$redirect->save();
		return $redirect;
	}

	/**
	 * Retrieve and old link
	 *
	 * @param string $domain The path to search
	 * @param string $accountId The account ID that the link belongs to
	 *
	 * @return \ClubFace\OldLink|null The old link object or null if nothing
	 *		is found
	 */
	public static function findByPath($path, $accountId)
	{
		return self::where('account_id', '=', $accountId)
			->where('url', '=', $path)
			->where('active', '=', true)
			->first();
	}

}
