<?php namespace ClubFace;

class Account extends \ClubFace\Database\Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'accounts';

	/**
	 * The account the current request relates to
	 *
	 * @var \ClubFace\Account
	 */
	protected static $currentAccount = null;

	/**
	 * Get the account that the current request relates to
	 *
	 * @return \ClubFace\Account|null
	 */
	public static function current($account = null)
	{
		if (!is_null($account)) {
			if (($account instanceof \ClubFace\Account)) {
				return self::$currentAccount = $account;
			} else {
				throw new \Exception('An invalid account object was set in ' . __METHOD__);
			}
		}

		if (!is_null(self::$currentAccount)) {
			return self::$currentAccount;
		}

		return null;
	}

	/**
	 * Retrieve an account object based on domain name
	 *
	 * @param string $domain The FQDN to search
	 *
	 * @return \ClubFace\Account|null The account object or null if no account is
	 *		associated with the specified domain
	 */
	public static function findByDomain($domain)
	{
		return self::whereIn('domains', [$domain])->first();
	}

	/**
	 * Determine whether or not a domain is associated with an account
	 *
	 * @param string $domain The FQDN to search
	 *
	 * @return bool Whether or not the domain is in use
	 */
	public static function domainInUse($domain)
	{
		return !is_null(self::findByDomain($domain));
	}

	public static function create(array $arguments)
	{
		if (!is_array($arguments) || !array_key_exists('name', $arguments) || !array_key_exists('domain', $arguments) ||
			!array_key_exists('domains', $arguments) || !array_key_exists('owner', $arguments)) {
			return null;
		}

		$account = new \ClubFace\Account;
		$account->domain = (string) $arguments['domain'];
		$account->domains = (array) $arguments['domains'];
		$account->suspended = false; 
		$account->owner = (string) $arguments['owner'];
		$account->club = [
			'name' => (string) $arguments['name'],
			'phone_numbers' => [],
			'email' => '',
			'address' => [
				'line1' => '',
				'line2' => '',
				'suburb' => '',
				'state' => '',
				'postcode' => ''
			],
			'logo' => [
				'small' => '',
				'medium' => '',
				'large' => '',
			],
		];
		$account->theme = \ClubFace\Themes::ONE;
		$account->save();
		return $account;
	}

}
