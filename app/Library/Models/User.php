<?php namespace ClubFace;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends \ClubFace\Database\Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public static function create(array $arguments)
	{
		if (!is_array($arguments) || !array_key_exists('account_id', $arguments) || !array_key_exists('name', $arguments) ||
			!array_key_exists('email', $arguments) || !array_key_exists('password', $arguments)) {
			return null;
		}

		$user = new \ClubFace\User;
		$user->account_id = (string) $arguments['account_id'];
		$user->name = (string) $arguments['name'];
		$user->email = (string) $arguments['email'];
		$user->password = (string) \Hash::make($arguments['password']);
		$user->active = true;
		$user->save();
		return $user;
	}

}
