<?php namespace ClubFace;

class Block extends \ClubFace\Database\Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blocks';

	public static function getBlockClass($id)
	{
		$theme = \ClubFace\Themes::getCurrentTheme();
		if (!$theme->blockIsSupported($id)) {
			return null;
		}

		$className = '\ClubFace\Themes\\' . \ClubFace\Themes::getCurrentTheme()->getId() . '\Blocks\\' . $id;
		if (!class_exists($className)) {
			$className = '\ClubFace\Blocks\\' . $id;
		}

		return new $className;
	}

	public static function activate($id)
	{
		$theme = \ClubFace\Themes::getCurrentTheme();
		if (!$theme->blockIsSupported($id)) {
			return null;
		}

		$blockClass = self::getBlockClass($id);
		$block = new \ClubFace\Block;
		$block->class = $id;
		$block->configuration = $blockClass->getDefaultConfig();
		$block->account_id = \ClubFace\Account::current()->_id;
		$block->save();
		return $block;
	}
	
}
