<?php namespace ClubFace;

class Page extends \ClubFace\Database\Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pages';

	public static function create(array $arguments)
	{
		if (!is_array($arguments) || !array_key_exists('account_id', $arguments) || !array_key_exists('url', $arguments) ||
			!array_key_exists('title', $arguments) || !array_key_exists('social', $arguments) || !array_key_exists('blocks', $arguments) ||
			!array_key_exists('created_by', $arguments)) {
			return null;
		}

		$page = new \ClubFace\Page;
		$page->account_id = (string) $arguments['account_id'];
		$page->url = (string) $arguments['url'];
		$page->title = (string) $arguments['title'];
		$page->social = (array) $arguments['social'];
		$page->created_by = (string) $arguments['created_by'];
		$page->blocks = (array) $arguments['blocks'];
		$page->css_file = null;
		$page->css_overrides = null;
		$page->active = true;
		$page->save();
		return $page;
	}

	public static function render($url)
	{
		$page = self::where('account_id', '=', \ClubFace\Account::current()->_id)
			->where('active', '=', true)
			->where('url', '=', $url)
			->first();

		if (is_null($page)) {
			return \Response::view('errors.404', [], 404);
		}

		return \Response::view('page', $page->attributesToArray(), 200);
	}
	
}
