<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['domain' => 'www.clubface.com.au'], function()
{
	/* No authentication required */
	Route::get('/', 'WelcomeController@index');

	/* Must be a guest */
	Route::group(['middleware' => 'guest'], function()
	{
		Route::get('/auth/login', 'AuthController@login');
		Route::post('/auth/login', 'AuthController@processlogin');
	});

	Route::group(['middleware' => 'auth'], function()
	{
		Route::get('/logout', 'AuthController@logout');

		Route::get('/dashboard', function() {
			echo 'DASHBOARD';
		});

		Route::get('/test', function() {
			$block = \ClubFace\Block::activate(\ClubFace\Blocks\BasicContent::ID);

		});

	});

});







/* Our wildcard URL catch */
Route::any('{url}', function($url) {
	/* TODO - Show a standard 404 if the domain is www.clubface.com.au */
	if (\Request::root() == 'https://www.clubface.com.au') {
		return \Response::view('errors/clubface_404', [], 404);
	}

	/* Hand the reqest over to the Sites handler */
	return Site::marshall($url);

})->where('url', '([A-z\d-\/_.]+)?');

