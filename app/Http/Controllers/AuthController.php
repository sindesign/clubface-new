<?php namespace ClubFace\Http\Controllers;

class AuthController extends Controller {

	public function login()
	{
		return view('login');
	}

	public function processlogin()
	{
		if (!\Input::has('email') || !\Input::has('password')) {
			return redirect('/auth/login');
		}

		if (\Auth::attempt(['email' => \Input::get('email'), 'password' => \Input::get('password'), 'active' => true])) {
            return redirect()->intended('dashboard');
        } else {
        	return redirect('/auth/login'); 
        }
	}

	public function logout()
	{
		\Auth::logout();
		return redirect('/auth/login?logout=sucessful'); 
	}

}
